---
title: 'Using Parrot'
taxonomy:
    category:
        - docs
child_type: docs
---

### Chapter 5

# Using Parrot

Learn to operate and configure the Parrot operating system
