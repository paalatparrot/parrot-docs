---
title: ARM
taxonomy:
    category:
        - docs
child_type: docs
---

### Chapter 4

# ARM

Deploy Parrot on an array of ARM devices

_<i class="fa fa-warning"></i>Parrot ARM builds are currently in the experimental stage._
