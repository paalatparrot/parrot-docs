---
title: 'Parrot Project'
taxonomy:
    category:
        - docs
child_type: docs
---

### Chapter 7

# Parrot Project

Information about the organization behind Parrot Linux
