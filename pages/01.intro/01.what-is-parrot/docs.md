---
title: 'What is Parrot?'
taxonomy:
    category:
        - docs
visible: true
---

**Parrot** (also Parrot Security, Parrot OS, Parrot GNU/Linux) is a Debian-based, security-oriented distribution featuring a collection of utilities designed for penetration testing, computer forensics, reverse engineering, hacking, privacy, anonymity and cryptography. The operating system comes with MATE as the default desktop environment and is released in three main versions preconfigured for security, home, and ARM development uses. 

The first release was on April 10th, 2013 as Parrot Security OS, the result of the work of Lorenzo Faletra who continues to lead development. Originally run as part of the now-defunct Frozenbox Network, today the community has grown to include a worldwide collection of open source developers, advocates of digital privacy, and Linux enthusiasts. The project is headquartered in Palermo, Italy with contributors located in over a dozen countries. 