---
title: Introduction
media_order: logo.png
taxonomy:
    category:
        - docs
child_type: docs
---

### Chapter 1

# Introduction

Getting started with the Parrot operating system