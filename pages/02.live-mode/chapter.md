---
title: 'Live Mode'
taxonomy:
    category:
        - docs
child_type: docs
---

### Chapter 2

# Live Mode

Running Parrot in a live environment
