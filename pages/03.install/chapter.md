---
title: Installation
taxonomy:
    category:
        - docs
child_type: docs
---

### Chapter 3

# Installation

Install and configure Parrot in different environments
